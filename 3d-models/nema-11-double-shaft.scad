/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

include <configuration.scad>

slop = 0.2;

m2_5_clearance_radius = 2.7 / 2;
front_bolt_center_to_center = 23;

front_shaft_length = 18;
front_shaft_radius = 5 / 2;
front_big_circle_radius = 22 / 2 + slop;
front_big_circle_depth = 2;
footprint_side_length = 28.2 + slop;
depth = 32 + slop;

encoder_radius = 21.75 / 2 + slop;
encoder_depth = 13.85 + slop;

function nema_11_footprint_side_length() = footprint_side_length;
function nema_11_front_shaft_length() = front_shaft_length;
function nema_11_holder_length() = depth + encoder_depth;
function nema_11_holder_height() =
  wall + footprint_side_length / 2;

//TODO: redo everything to center on x axis

module nema_11_body(){
  translate([0, 0, depth / 2]){
    cube([footprint_side_length, footprint_side_length, depth],
         center = true);}
  translate([0, 0, depth]){
    linear_extrude(height = front_big_circle_depth){
      circle(front_big_circle_radius);}}}

module nema_11_shaft(){
  linear_extrude(height = front_shaft_length){
    circle(front_shaft_radius);}}

module encoder(){
  linear_extrude(height = encoder_depth){
    circle(encoder_radius);}}

!nema_11();
module nema_11(){
  nema_11_body();
  translate([0, 0, depth + front_big_circle_depth]){
    nema_11_shaft();}
  translate([0, 0, -encoder_depth]){
    encoder();}}

//!nema_11_holder();
module nema_11_holder(){
  holder_length = front_big_circle_depth + depth + encoder_depth + wall;
  translate([wall + depth, 0, 0]){
    %rotate([0, -90, 0]){
      nema_11();}
    difference(){
      translate([-holder_length + encoder_depth + wall,
                 -(footprint_side_length / 2 + wall),
                 -(footprint_side_length / 2 + wall)]){
        linear_extrude(height = footprint_side_length / 2 + wall){
          square([holder_length, wall + footprint_side_length + wall]);}}
      rotate([0, -90, 0]){
        nema_11();}
      translate([-depth,
                 -front_bolt_center_to_center / 2,
                 -front_bolt_center_to_center / 2]){
        rotate([0, -90, 0]){
          linear_extrude(height = wall){
            circle(m2_5_clearance_radius);}}}
      translate([-depth,
                 front_bolt_center_to_center / 2,
                 -front_bolt_center_to_center / 2]){
        rotate([0, -90, 0]){
          linear_extrude(height = wall){
            circle(m2_5_clearance_radius);}}}}}}

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

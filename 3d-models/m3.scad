/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

/*
  TODO
  make a single top down square nut trap
  and hexagon thread hole module
 */

function m3_sq_width() = 5.5;
function m3_sq_thickness() = 1.8;

function m3_thread_radius() = 3 / 2;
function m3_socket_head_radius() = 5.68 / 2;

function hexagon_factor() = 1 / cos(360 / 12);
function m3_hexagon_thread_radius() = m3_thread_radius() * hexagon_factor();

nut_wiggle_room = 0.15;

//m3_nut_trap_with_flare(10);
module m3_nut_trap_with_flare(depth){
  translate([m3_sq_thickness() / 2, m3_sq_width() / 2, 0]){
    m3_nut_trap(depth);
    translate([0, 0, 2]){
      rotate([0, 180, 0]){
        m3_nut_flare(2);}}}}

//!m3_sq_top_down_trap(10);
module m3_sq_top_down_trap(depth){
  linear_extrude(height = depth){
    minkowski(){
      square(m3_sq_width(), center = true);
      circle(r = nut_wiggle_room);}}
  linear_extrude(height = 1, scale = 0.9){
    minkowski(){
      square(m3_sq_width()/(0.9), center = true);
      circle(r = nut_wiggle_room);}}}

module m3_nut_trap(depth){
  linear_extrude(height = depth){
    minkowski(){
      square([m3_sq_thickness(), m3_sq_width()], center = true);
      circle(r = nut_wiggle_room);}}}

module m3_nut_flare(depth){
  linear_extrude(height = depth, scale = 1.5){
    minkowski(){
      square([m3_sq_thickness(), m3_sq_width()], center = true);
      circle(r = nut_wiggle_room);}}}

////////////////////////////////////////////////////////////////////
/* http://thenativecreative.co/socket-head-cap-screws-dimensions/ */
////////////////////////////////////////////////////////////////////

// d
function m3_body_radius() = 3 / 2;

// dk
function m3_head_radius() = 5.68 / 2;
function m3_hexagon_head_radius() = m3_head_radius() * hexagon_factor();

// k
function m3_head_depth() = 3;

//m3_socket_cap_trap(10, 20);
module m3_socket_cap_trap(head_depth, body_length){
  cylinder(h = body_length,
           r = m3_body_radius() * hexagon_factor(),
           $fn = 6);
  translate([0, 0, body_length]){
    cylinder(h = head_depth,
             r = m3_head_radius() * hexagon_factor(),
             $fn = 6);}}

module m3_thread_hole(length){
  linear_extrude(length){
    circle(m3_thread_radius());}}

module m3_hexagon_thread_hole(length){
  linear_extrude(length){
    circle(m3_hexagon_thread_radius(), $fn = 6);}}

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

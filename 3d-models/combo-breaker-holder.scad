/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

include <configuration.scad>
use <nema-11-double-shaft.scad>
use <master-lock.scad>
use <servo.scad>
use <m3.scad>

module combo_breaker_holder(){
  // TODO: why is lock holder not flush with connector block
  %translate([-(wall + nema_11_front_shaft_length()), 0, 0]){
    lock_holder();}

  // connector block
  module connector_block(){
    translate([-(nema_11_front_shaft_length()
                 + master_dial_knob_height()
                 + wall
                 + (master_dial_base_height() - wall)),
               -(wall + nema_11_footprint_side_length() / 2),
               -(nema_11_holder_height()
                 + master_dial_center_to_bottom()
                 - nema_11_footprint_side_length() / 2)]){
      cube([(wall + nema_11_holder_length() + wall)
            + nema_11_front_shaft_length()
            + master_dial_knob_height()
            + wall
            + (master_dial_base_height() - wall),
            wall + nema_11_footprint_side_length() + wall,
            master_dial_center_to_bottom()
            - nema_11_footprint_side_length() / 2]);}}

  module left_bolt_fixture(){
    translate([-(nema_11_front_shaft_length()
                 + wall
                 + master_dial_knob_height()
                 + (master_dial_base_height() - wall)),
               -(master_lock_holder_width() / 2),
               -(master_dial_center_to_bottom()
                 + wall)]){
      difference(){
        cube([wall + m3_head_depth(),
              (master_lock_holder_width()
               - (wall * 2 + nema_11_footprint_side_length())) / 2,
              wall * 2 + m3_sq_width() + wall * 2]);
        translate([0,
                   wall + m3_sq_width() / 2,
                   wall * 2 + m3_sq_width() / 2]){
          rotate([0, 90, 0]){
            m3_socket_cap_trap(m3_head_depth(), wall);}}}}}

  module servo_support(){
    translate([-(nema_11_front_shaft_length()
                 + wall
                 + master_dial_knob_height()
                 + (master_dial_base_height() - wall)),
               nema_11_footprint_side_length() / 2 + wall,
               -(master_dial_center_to_bottom()
                 + wall)]){
      difference(){
        cube([servo_width(),
              servo_support_length()
              + ((horn_depth() / 2 + (servo_shoulder_depth()
                                      + servo_arm_back_from_front()
                                      + servo_arm_thickness()))
                 - (nema_11_footprint_side_length() / 2 + wall)),
              wall
              + master_dial_center_to_bottom()
              + (middle_of_top_of_shackle()
                 - servo_horn_height()
                 - servo_arm_length())]);
        // lower right hole
        translate([0,
                   servo_support_length()
                   + ((horn_depth() / 2 + (servo_shoulder_depth()
                                           + servo_arm_back_from_front()
                                           + servo_arm_thickness()))
                      - (nema_11_footprint_side_length() / 2 + wall))
                   - (wall * 2 + m3_sq_width() / 2),
                   wall * 2 + m3_sq_width() / 2]){
          rotate([90, 0, 90]){
            m3_socket_cap_trap(m3_head_depth(),
                               (servo_width() - m3_head_depth()));}}
        // upper right hole
        translate([0,
                   servo_support_length()
                   + ((horn_depth() / 2 + (servo_shoulder_depth()
                                           + servo_arm_back_from_front()
                                           + servo_arm_thickness()))
                      - (nema_11_footprint_side_length() / 2 + wall))
                   - (wall * 2 + m3_sq_width() / 2),
                   wall
                   + master_dial_center_to_bottom()
                   + (middle_of_top_of_shackle()
                      - servo_horn_height()
                      - servo_arm_length())
                   - (wall * 2 + m3_sq_width() / 2)]){
          rotate([90, 0, 90]){
            m3_socket_cap_trap(m3_head_depth(),
                               (servo_width() - m3_head_depth()));}}}}

    translate([-(nema_11_front_shaft_length()
                 + wall
                 + master_dial_knob_height()
                 + (master_dial_base_height() - wall)),
               horn_depth() / 2,
               middle_of_top_of_shackle()
               - servo_horn_height()]){
      %servo();
      under_servo_support();}}

  nema_11_holder();
  connector_block();
  left_bolt_fixture();
  servo_support();}

combo_breaker_holder();

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

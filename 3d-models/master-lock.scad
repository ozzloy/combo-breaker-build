/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

include <configuration.scad>
use <servo.scad>
use <m3.scad>

/*
  https://www.masterlock.com/personal-use/product/1500D
 */

A = 7.2 * (7 / 6.87) + 0.4;
function shackle_arm_radius() = A / 2;
B = 19 * (19 / 18.88);
C = 20.56 * (21 / 21.13) - 0.4;
width = 48.2 * (48/47.52) + 0.2;

shackle_outer = C + A * 2.0;
shackle_inner = C;
shackle_arm_length = width / 2 + B - C / 2;
shackle_up_from_bottom = 6.5;
shackle_trap_depth = A / 2 + shackle_up_from_bottom;

lock_depth = 20;

dial_base_diameter = 37;
dial_base_upper_diameter = 20;
dial_base_height = 2.5;
dial_down_from_center = 3.5;

dial_knob_height = 10.8;

function middle_of_top_of_shackle() =
  width / 2 + B + A / 2 + dial_down_from_center;

function master_dial_center_to_bottom() =
  width / 2 - dial_down_from_center;
function master_dial_knob_height() = dial_knob_height;
function master_dial_base_height() = dial_base_height;

module lock_body_outline(){
  circle(d = width);}

module shackle_arch_outline(){
  translate([0, width / 2 + shackle_arm_length]){
    difference(){
      circle(d = shackle_outer);
      circle(d = shackle_inner);
      translate([0, -shackle_outer / 2]){
        square(shackle_outer, center = true);}}}}

module shackle_arm_outline(){
  square([A, shackle_arm_length + width / 2.0], center = true);}

module shackle_arms_outline(){
  translate([-(C + A) / 2.0, width / 4.0 + shackle_arm_length / 2.0]){
    shackle_arm_outline();}
  translate([(C + A) / 2.0, width / 4.0 + shackle_arm_length / 2.0]){
    shackle_arm_outline();}}

module shackle_outline(){
  shackle_arch_outline();
  shackle_arms_outline();}

module lock_outline(){
  shackle_outline();
  lock_body_outline();}

housing_height = width + B + A + wall * 2;
housing_width = width + wall * 2;
module housing_outline(){
  difference(){
    translate([0, housing_height / 2 - width / 2 - wall]){
      square([housing_width, housing_height], center = true);}
    lock_outline();}}

module simple_lock_housing(){
  linear_extrude(height = 10){
    housing_outline();}
  translate([0, housing_height / 2 - width / 2 - wall, -10]){
    linear_extrude(height = 10){
      square([housing_width, housing_height], center = true);}}}

module lock_body(){
  linear_extrude(height = lock_depth){
    lock_body_outline();}}

//!shackle_arm_2d();
module shackle_arm_2d(){
  square([A, shackle_arm_length]);}

//!shackle_arm();
module shackle_arm(){
  rotate([-90, 0, 0]){
    cylinder(h = shackle_arm_length, d = A);}}

//!shackle_arms_2d();
module shackle_arms_2d(){
  translate([C / 2, 0]){
    shackle_arm_2d();}
  translate([-A - C / 2, 0]){
    shackle_arm_2d();}}

//!shackle_arms();
module shackle_arms(){
  translate([-(C + A) / 2, 0]){
    shackle_arm();}
  translate([(C + A) / 2, 0]){
    shackle_arm();}}

//!shackle_arch_2d();
module shackle_arch_2d(){
  outer_radius = A + C / 2;
  inner_radius = C / 2;
  difference(){
    circle(outer_radius);
    circle(inner_radius);
    translate([-outer_radius, -outer_radius]){
      square([2*outer_radius, outer_radius]);}}}

//!shackle_2d();
module shackle_2d(){
  translate([0, shackle_arm_length]){
    shackle_arch_2d();}
  shackle_arms_2d();}

//!shackle_arch();
module shackle_arch(){
  rotate_extrude(angle = 180){
    translate([(C + A) / 2, 0]){
      circle(d = A);}}}

//#shackle();
module shackle(){
  shackle_arms();
  translate([0, (width - C) / 2 + B]){
    shackle_arch();}}

module dial(){
  dial_base();
  dial_knob();}

module dial_base(){
  cylinder(d1 = dial_base_diameter,
           d2 = dial_base_upper_diameter,
           h = dial_base_height);}

module dial_knob(){
  dial_knob_base_diameter = dial_base_upper_diameter;
  dial_knob_upper_diameter = 18;
  translate([0, 0, dial_base_height]){
    cylinder(d1 = dial_knob_base_diameter,
             d2 = dial_knob_upper_diameter,
             h = dial_knob_height);}}

//!lock();
module lock(){
  rotate([90, 0, -90]){
    translate([0,
               0,
               -(lock_depth
                 + dial_base_height
                 + dial_knob_height)]){
      translate([0, 0, lock_depth]){
        dial();}
      translate([0, dial_down_from_center, 0]){
        lock_body();
        translate([0, 0, A / 2 + shackle_up_from_bottom]){
          shackle();}}}}}

//!shackle_trap();
module shackle_trap(){
  linear_extrude(height = shackle_trap_depth){
    shackle_2d();}}

//!simple_lock_holder();
module simple_lock_holder(){
  holder_height = wall + A + B + width + wall;
  holder_depth =
    wall + lock_depth + dial_base_height + dial_knob_height;
  holder_width = wall + width + wall;


  difference(){
    cube([holder_width, holder_depth, holder_height]);
    translate([wall + width / 2,
               wall,
               wall + width / 2 - dial_down_from_center]){
      rotate(90){
        lock();}}
    translate([wall + width / 2,
               holder_depth,
               wall + width / 2]){
      rotate([90, 0, 0]){
        shackle_trap();}}}}

/* difference(){ */
/*   rotate([0, 90, 0]){lock_holder();} */
/*   translate([0, 0, 69]){cube(100, center=true);}} */
rotate([0, 90, 0]){lock_holder();}
/* lock_holder(); */
holder_height = width + B - C / 2 + wall;
holder_width = wall + width + wall;
function master_lock_holder_width() = holder_width;
holder_depth = wall + lock_depth;
module lock_holder(){
  servo_hole_width = horn_depth() + wall * 2;
  translate([-(lock_depth + dial_base_height + dial_knob_height),
             -holder_width / 2,
             dial_down_from_center - holder_width / 2]){
    %translate([lock_depth + dial_base_height + dial_knob_height,
                wall + width / 2,
                wall + width / 2 - dial_down_from_center]){
      rotate([0, 0, 180]){
        lock();}}
    // main lock holder
    difference(){
      union(){
        cube([holder_depth, holder_width, holder_height]);
        // servo connection
        servo_connection_depth = wall + m3_sq_thickness();
        translate([holder_depth - servo_connection_depth,
                    (wall + width + horn_depth()) / 2
                    + servo_shoulder_depth()
                    + servo_arm_back_from_front()
                    + servo_arm_thickness()
                    //TODO: where does this 1 come from?!?
                    + 1,
                    0]){
          difference(){
            cube([servo_connection_depth,
                  servo_support_length(),
                  holder_height]);
            // lower hole
            translate([0,
                       servo_support_length()
                       - (m3_sq_width() / 2 + wall * 2),
                       wall * 2 + m3_sq_width() / 2]){
              rotate([0, 90, 0]){
                m3_sq_top_down_trap(m3_sq_thickness());}
              rotate([90, 0, 90]){
                m3_hexagon_thread_hole(wall + m3_sq_thickness());}}
            // upper hole
            translate([0,
                       servo_support_length()
                       - (m3_sq_width() / 2 + wall * 2),
                       wall
                       + master_dial_center_to_bottom()
                       + (middle_of_top_of_shackle()
                          - servo_horn_height()
                          - servo_arm_length())
                       - (wall * 2 + m3_sq_width() / 2)]){
              rotate([0, 90, 0]){
                m3_sq_top_down_trap(m3_sq_thickness());}
              rotate([90, 0, 90]){
                m3_hexagon_thread_hole(wall + m3_sq_thickness());}}}}}

      // window to see shackle lockedness
      translate([0, -C / 2, 12 + holder_height / 2]){
        difference(){
          cube([holder_depth, holder_width / 2, holder_height / 2]);
          rotate([-47, 0, 0]){
            translate([0, (holder_width - C) / 2 - 3, 0]){
              cube([holder_depth, holder_width / 2, holder_height / 2]);}}}}

      // spot indicator
      translate([holder_depth - wall,
                 holder_width / 2 - wall / 2,
                 wall + (width / 2
                         - dial_down_from_center)
                   + (dial_base_diameter / 2 - 0.1)]){
        cube([wall, wall, wall * 2]);}
      translate([0,
                 (holder_width - servo_hole_width) / 2,
                 width + 2 * wall]){
        cube([holder_depth, servo_hole_width, holder_height]);}
      translate([lock_depth + dial_base_height + dial_knob_height,
                 wall + width / 2,
                 wall + width / 2 - dial_down_from_center]){
        rotate([0, 0, 180]){
          lock();}}
      translate([0, holder_width / 2, holder_width / 2]){
        rotate([90, 0, 90]){
          shackle_trap();}}
      translate([0,
                 holder_width / 2,
                 holder_width / 2 - dial_down_from_center]){
        rotate([0, 90, 0]){
          cylinder(h = 100, d = dial_base_diameter);}}
      // m3 nut traps and flares on back of holder
      translate([m3_sq_thickness() / 2 + wall * 2,
                 0,
                 m3_sq_width() / 2 + wall * 2]){
        rotate([-90, 0, 0]){
          m3_nut_trap(m3_sq_width() + wall);}}
      translate([m3_sq_thickness() / 2 + wall * 2,
                 wall,
                 m3_sq_width() / 2 + wall * 2]){
        rotate([90, 0, 0]){
          m3_nut_flare(wall);}}
      translate([m3_sq_thickness() / 2 + wall * 2,
                 holder_width,
                 m3_sq_width() / 2 + wall * 2]){
        rotate([-90, 0, 180]){
          m3_nut_trap(m3_sq_width() + wall);}}
      translate([m3_sq_thickness() / 2 + wall * 2,
                 holder_width - wall,
                 m3_sq_width() / 2 + wall * 2]){
        rotate([90, 0, 180]){
          m3_nut_flare(wall);}}
      translate([m3_sq_thickness() / 2 + wall * 2,
                 holder_width,
                 holder_height - (m3_sq_width() / 2 + wall * 2)]){
        rotate([-90, 0, 180]){
          m3_nut_trap(m3_sq_width() + wall);}}
      translate([m3_sq_thickness() / 2 + wall * 2,
                 holder_width - wall,
                 holder_height - (m3_sq_width() / 2 + wall * 2)]){
        rotate([90, 0, 180]){
          m3_nut_flare(wall);}}

      // m3 nut traps and flares on front of holder
      translate([holder_depth - (m3_sq_thickness() / 2 + wall * 2),
                 0,
                 m3_sq_width() / 2 + wall * 2]){
        rotate([-90, 0, 0]){
          m3_nut_trap(m3_sq_width() + wall);}}
      translate([holder_depth - (m3_sq_thickness() / 2 + wall * 2),
                 wall,
                 m3_sq_width() / 2 + wall * 2]){
        rotate([90, 0, 0]){
          m3_nut_flare(wall);}}

      // m3 bolt holes
      translate([0,
                 m3_sq_width() / 2 + wall,
                 m3_sq_width() / 2 + wall * 2]){
        rotate([0, 90, 0]){
          m3_hexagon_thread_hole(holder_depth);}}
      translate([0,
                 holder_width - (m3_sq_width() / 2 + wall),
                 m3_sq_width() / 2 + wall * 2]){
        rotate([0, 90, 0]){
          m3_hexagon_thread_hole(holder_depth);}}
      translate([0,
                 holder_width - (m3_sq_width() / 2 + wall),
                 holder_height - (m3_sq_width() / 2 + wall * 2)]){
        rotate([0, 90, 0]){
          m3_hexagon_thread_hole(holder_depth);}}}}}

//holder_back();
module holder_back(){
  linear_extrude(wall){
    difference(){
      square([holder_width, holder_height]);
      translate([m3_sq_width() / 2 + wall,
                 m3_sq_width() / 2 + wall * 2]){
        rotate(90){
          circle(m3_hexagon_thread_radius(), $fn = 6);}}
      translate([holder_width - (m3_sq_width() / 2 + wall),
                 m3_sq_width() / 2 + wall * 2]){
        rotate(90){
          circle(m3_hexagon_thread_radius(), $fn = 6);}}
      translate([m3_sq_width() / 2 + wall,
                 holder_height - (m3_sq_width() / 2 + wall * 2)]){
        rotate(90){
          circle(m3_hexagon_thread_radius(), $fn = 6);}}
      translate([holder_width - (m3_sq_width() / 2 + wall),
                 holder_height - (m3_sq_width() / 2 + wall * 2)]){
        rotate(90){
          circle(m3_hexagon_thread_radius(), $fn = 6);}}}}
  translate([0, 0, wall]){
    linear_extrude(m3_head_depth()){
      difference(){
        square([holder_width, holder_height]);
        translate([m3_sq_width() / 2 + wall,
                   m3_sq_width() / 2 + wall * 2]){
          rotate(90){
            circle(m3_hexagon_head_radius(), $fn = 6);}}
        translate([holder_width - (m3_sq_width() / 2 + wall),
                   m3_sq_width() / 2 + wall * 2]){
          rotate(90){
            circle(m3_hexagon_head_radius(), $fn = 6);}}
        translate([m3_sq_width() / 2 + wall,
                   holder_height - (m3_sq_width() / 2 + wall * 2)]){
          rotate(90){
            circle(m3_hexagon_head_radius(), $fn = 6);}}
        translate([holder_width - (m3_sq_width() / 2 + wall),
                   holder_height - (m3_sq_width() / 2 + wall * 2)]){
          rotate(90){
            circle(m3_hexagon_head_radius(), $fn = 6);}}}}}}

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

include <configuration.scad>
use <m3.scad>
use <master-lock.scad>

width = 19.40;
function servo_width() = width;
depth = 38.4;
height = 39.8;

center_to_center = 10;
arm_hole_radius = 4.85 / 2;
out_from_body = 2.15;
arm_back_from_front = 8.65;
function servo_arm_back_from_front() = arm_back_from_front;
arm_thickness = 2.5;
function servo_arm_thickness() = arm_thickness;
arm_hole_gap = 5.1;
arm_hole_z_gap = 43.35;
arm_length = 8;
function servo_arm_length() = arm_length;
armspan = arm_length * 2 + height;

shoulder_depth = 1;
function servo_shoulder_depth() = shoulder_depth;
shoulder_radius = 12.25 / 2;

head_depth = 4.25;
head_outer_radius = 6 / 2 + 0.325;
head_inner_radius = 5.5 / 2 + 0.025;
head_down_from_top = 6.53 + head_outer_radius;
head_tooth_count = 23;

horn_length = 25;
horn_outer_radius = head_outer_radius + wall * 3;
horn_depth = m3_sq_width();
function horn_depth() = horn_depth;
horn_screw_radius = m3_thread_radius();

function servo_horn_height() = height - head_down_from_top;

//servo_body();
module servo_body(){
  cube([width, depth, height]);}

//!arm_2d();
module arm_2d(){
  difference(){
    square([width / 2, arm_length]);
    translate([arm_hole_gap / 2 + arm_hole_radius,
               (arm_hole_z_gap - height) / 2 + arm_hole_radius]){
      circle(arm_hole_radius);}}}

module mirrored(v){
  children();
  mirror(v){
    children();}}

//!arms_2d();
module arms_2d(){
  mirrored([0, 1]){
    mirrored([1, 0]){
      translate([0, height / 2]){
        arm_2d();};}}}

//!arms();
module arms(){
  translate([width / 2, 0]){
    linear_extrude(height = arm_thickness){
      arms_2d();}}}

module shoulder_2d(){
  circle(shoulder_radius);}

module shoulder(){
  linear_extrude(height = shoulder_depth){
    shoulder_2d();}}

//!head_2d(); // c
module head_2d(){
  tooth_angle = 360 / head_tooth_count;
  circle(head_inner_radius);
  for(tooth = [0 : head_tooth_count - 1]){
    rotate(tooth * tooth_angle){
      polygon([[0, 0],
               [cos(tooth_angle / 2) * head_inner_radius,
                sin(tooth_angle / 2) * head_inner_radius],
               [cos(tooth_angle / 4) * head_inner_radius,
                sin(tooth_angle / 4) * head_inner_radius],
               [cos(tooth_angle / 5) * head_outer_radius,
                sin(tooth_angle / 5) * head_outer_radius],
               [head_outer_radius, 0],
               [cos(tooth_angle / 5) * head_outer_radius,
                -sin(tooth_angle / 5) * head_outer_radius],
               [cos(tooth_angle / 4) * head_inner_radius,
                -sin(tooth_angle / 4) * head_inner_radius],
               [cos(tooth_angle / 2) * head_inner_radius,
                -sin(tooth_angle / 2) * head_inner_radius]]);}}}

//!head();
module head(){
  linear_extrude(height = head_depth){
    head_2d();}}

clamp_length = m3_sq_width() + wall;
//horn_2d_outline();
module horn_2d_outline(){
  hull(){
    translate([-horn_length, 0]){
      circle(horn_outer_radius);}
    translate([head_outer_radius, -horn_outer_radius]){
      square([clamp_length, horn_outer_radius * 2]);}}}

//horn_2d();
clamp_gap = wall;
module horn_2d(){
  difference(){
    horn_2d_outline();
    translate([-(horn_outer_radius + horn_length), -clamp_gap/2]){
      square([(horn_outer_radius
               + horn_length
               + head_outer_radius
               + clamp_length),
              clamp_gap]);}
    translate([-(horn_length + horn_outer_radius),0]){
      hull(){
        square(shackle_arm_radius() * 2 + 1, center = true);
        translate([horn_outer_radius + horn_length
                   - (head_outer_radius
                      + m3_sq_width()
                      + wall
                      + shackle_arm_radius()
                      + 1), 0]){
          circle(shackle_arm_radius() + 1);}}}
    head_2d();}}

/* translate([0, 0, horn_depth]){ */
/*   rotate([180, 0, 0]){ */
/*     horn();}} */
horn();
module horn(){
  difference(){
    linear_extrude(height = horn_depth){
      horn_2d();}
    mirrored([1, 0, 0]){
      mirrored([0, 1, 0]){
        translate([m3_sq_width() / 2 + head_outer_radius,
                   horn_outer_radius,
                   horn_depth / 2]){
          rotate([90, 0, 0]){
            m3_sq_top_down_trap(m3_sq_thickness());
            m3_hexagon_thread_hole(horn_outer_radius);}}}}}}

//!half_servo_support_2d();
module half_servo_support_2d(){
  horn_screw_hexagon_radius = horn_screw_radius * hexagon_factor();
  difference(){
    square([width / 2, arm_length]);
    translate([arm_hole_gap / 2 + arm_hole_radius,
               (arm_hole_z_gap - height) / 2 + arm_hole_radius]){
      rotate(90){
        circle(horn_screw_hexagon_radius, $fn = 6);}}}}

//!servo_support_2d();
module servo_support_2d(){
  mirrored([1, 0]){
    half_servo_support_2d();}}

/* servo_support(); */
support_length = depth - arm_back_from_front - arm_thickness;
function servo_support_length() = support_length;
module servo_support(){
  difference(){
    linear_extrude(height = support_length){
      servo_support_2d();}
    translate([-width / 2,
               (arm_hole_z_gap - height) / 2 + arm_hole_radius,
               wall * 2]){
      rotate([0, 90, 0]){
        m3_nut_trap(width);}}}}

/* color("darkblue"){ */
/*   under_servo_support();} */
function servo_shoulder_depth() = shoulder_depth;
function servo_arm_back_from_front() = arm_back_from_front;
function servo_arm_thickness() = arm_thickness;
module under_servo_support(){
  translate([width / 2,
             shoulder_depth + arm_back_from_front + arm_thickness,
             0]){
    rotate([-90, 0, 0]){
      servo_support();}}}

//!servo();
module servo(){
  translate([0, shoulder_depth, 0]){
    servo_body();
    translate([0, arm_thickness + arm_back_from_front, height / 2]){
      rotate([90, 0, 0]){
        arms();}}
    translate([width / 2, 0, servo_horn_height()]){
      rotate([90, 0, 0]){
        shoulder();
        translate([0, 0, shoulder_depth]){
          head();
          horn();}}}}}

/* color("purple"){ */
/*   servo();} */

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

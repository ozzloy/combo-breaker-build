/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

include <configuration.scad>
use <m3.scad>

dial_top_radius = 17.33 / 2 + 0.4;
dial_base_radius = 17.9 / 2 + 0.4;
dial_base_teeth_radius = 18.7 / 2 + 0.3;
dial_teeth_height = 7;
dial_no_teeth_height = 2;
// how many times bigger are teeth than the inner radius
dial_tooth_factor = dial_base_teeth_radius / dial_base_radius;
dial_teeth = 40;
dial_nose_height = 2.6;
dial_height =
  dial_teeth_height + dial_nose_height;

shaft_radius = 5 / 2 + 0.2;
shaft_length = 18;

// rough sketch with no teeth
//dial_with_no_teeth();
module dial_with_no_teeth(){
  cylinder(r1 = dial_base_radius,
           r2 = dial_top_radius,
           h = dial_teeth_height);}

/* dial_tooth_2d(dial_teeth, */
/*               dial_tooth_factor, */
/*               dial_base_radius, */
/*               $fn = 50); */
module dial_tooth_2d(teeth, tooth_factor, radius){
  half_angle = (360 / teeth) / 2;
  outer_radius = radius * tooth_factor;
  difference(){
    polygon([[0, 0],
             [outer_radius * cos(half_angle),
              radius * sin(half_angle)],
             [outer_radius,
              0],
             [outer_radius * cos(half_angle),
              -radius * sin(half_angle)]]);
    translate([outer_radius * cos(half_angle),
               outer_radius * sin(half_angle)]){
      circle(outer_radius - radius);}
    translate([outer_radius * cos(half_angle),
               -outer_radius * sin(half_angle)]){
      circle(outer_radius - radius);}}}

/* dial_teeth_2d(dial_teeth, dial_tooth_factor, dial_base_radius); */
module dial_teeth_2d(teeth, tooth_factor, radius){
  // the circle should be redundant, but due to rounding errors,
  //  it is added to ensure the teeth are all part of 1 piece
  circle(radius);
  for(ii = [0:teeth - 1]){
    rotate(ii * 360 / teeth){
      dial_tooth_2d(teeth, tooth_factor, radius);}}}

/* dial(dial_teeth, */
/*      dial_tooth_factor, */
/*      dial_base_radius, */
/*      dial_top_radius, */
/*      dial_teeth_height, */
/*      $fn = 60); */
module dial(teeth, tooth_factor, base_radius, top_radius, height){
  taper_factor = top_radius / base_radius;
  linear_extrude(height = height, scale = taper_factor){
    dial_teeth_2d(teeth, tooth_factor, base_radius);}
  translate([0, 0, height]){
    linear_extrude(height = dial_nose_height){
      circle(dial_top_radius);}}}

/* shaft(shaft_length, shaft_radius); */
module shaft(length, radius){
  linear_extrude(height = length){
       circle(radius);}}

shaft_flare_height = 2;
module shaft_flare(length, radius){
  linear_extrude(height = length, scale = 1.5){
    circle(radius);}}

coupler_height = dial_height + wall + shaft_length / 2;
coupler_radius = dial_base_radius * dial_tooth_factor + wall;

module dial_stepper_shaft_coupler(){
  difference(){
    cylinder(r = dial_base_radius * dial_tooth_factor + wall,
             h = coupler_height);
    translate([0, 0, -dial_no_teeth_height]){
      dial(dial_teeth,
           dial_tooth_factor,
           dial_base_radius,
           dial_top_radius,
           dial_height);}
    translate([0, 0, dial_height + wall / 2]){
      shaft(shaft_length + wall / 2 + 0.001, shaft_radius);}
    translate([0, 0, coupler_height - shaft_flare_height]){
      shaft_flare(shaft_flare_height + 0.01, shaft_radius);}
    how_many_fingers = 9;
    finger_gap = 2;
    for(finger_index = [0 : how_many_fingers - 1]){
      rotate(180 + finger_index * 360 / how_many_fingers){
        translate([0, -finger_gap / 2]){
          linear_extrude(dial_height){
            square([dial_base_radius * dial_tooth_factor + wall,
                    finger_gap]);}}}}
    how_many_nut_traps = 3;
    for(m3_nut_trap_index = [0 : how_many_nut_traps - 1]){
      rotate(m3_nut_trap_index * 360 / how_many_nut_traps){
        translate([shaft_radius + m3_sq_thickness() / 2 + 0.8,
                   0,
                   dial_height + wall]){
          m3_nut_trap(100);}
        translate([shaft_radius + m3_sq_thickness() / 2 + 0.8,
                   0,
                   coupler_height - m3_sq_thickness()]){
          m3_nut_flare(m3_sq_thickness());}
        translate([shaft_radius - 1,
                   0,
                   dial_height + wall + m3_sq_width() / 2]){
          rotate([0, 90, 0]){
            m3_socket_cap_trap(10, coupler_radius
                                   - m3_head_depth()
                                   - shaft_radius);}}}}}}

dial_stepper_shaft_coupler();

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

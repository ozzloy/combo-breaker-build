/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

/*
  IN PROGRESS: make index 0 be back disk, zeroth number entered.
  index (disks - 1) should be dial face disk
*/

#include "pins.h"

#include "pitches.h"

boolean slow = false;
boolean debug = false;
/* skip detection needs to run very quickly,
   so debugging is extra guarded, even when normal debugging is on */
boolean skip_detect_debug = false;
/* stepper go-ing needs to run very quickly,
   so debugging is extra guarded, even when normal debugging is on */
boolean go_debug = false;
// indentation level, amount of space at beginning of debug output lines
int indent = 0;
int slow_spot_delay = 1000;

const int baudrate = 9600;

const int numbers_on_dial = 40;

#include <AccelStepper.h>
const int motor_interface_type = 1;
AccelStepper knob(motor_interface_type, step_pin, direction_pin);
const int steps_per_rotation = 200;
const int steps_per_number = steps_per_rotation / numbers_on_dial;
const int microsteps_per_step = 1;
const int knob_speed = slow ? 50 : 32000;
const unsigned int knob_acceleration = slow ? 2500 : 32000;

// https://www.pjrc.com/teensy/td_libs_Encoder.html
#include <Encoder.h>
const long count_per_rotation = 1200;
Encoder encoder(encoder_signal_a_pin, encoder_signal_b_pin);

const int count_per_step = count_per_rotation / steps_per_rotation;
const int count_step_radius = count_per_step / 2;

#include <Servo.h>
const int feedback_settle_time = 128;
Servo servo;
const int shackle_down_pulse = 130;
const int shackle_rest_position = 0;
const int shackle_open_position = 180;
const int shackle_rest_time = 200;
const int shackle_open_time = 250;
const int pulse_step = 5;
const int pulse_bottom = 0;
const int pulse_top = 180;
const int pulse_range = (pulse_top - pulse_bottom);
const int pulse_top_index = (pulse_range / pulse_step);
const int servo_rows = (pulse_range / pulse_step + 1);
const int servo_columns = 2;

int servo_pulse_feedback[servo_rows][servo_columns] =
  {{  0, 117},
   {  5, 128},
   { 10, 145},
   { 15, 159},
   { 20, 168},
   { 25, 178},
   { 30, 190},
   { 35, 200},
   { 40, 211},
   { 45, 220},
   { 50, 230},
   { 55, 244},
   { 60, 252},
   { 65, 266},
   { 70, 273},
   { 75, 287},
   { 80, 296},
   { 85, 307},
   { 90, 317},
   { 95, 325},
   {100, 338},
   {105, 347},
   {110, 358},
   {115, 368},
   {120, 381},
   {125, 390},
   {130, 400},
   {135, 408},
   {140, 418},
   {145, 430},
   {150, 438},
   {155, 448},
   {160, 457},
   {165, 468},
   {170, 478},
   {175, 486},
   {180, 499}};

#include <Keypad.h>

const byte rows = 4;
const byte columns = 4;
const char keys[rows][columns] =
  {{'1', '2', '3', 'A'},
   {'4', '5', '6', 'B'},
   {'7', '8', '9', 'C'},
   {'*', '0', '#', 'D'}};

byte row_pins[rows] = {53, 52, 51, 50};
byte column_pins[columns] = {49, 48, 47, 46};

Keypad keypad =
  Keypad(makeKeymap(keys),
         row_pins,
         column_pins,
         rows,
         columns);

// list of spots disks need to be in to open shackle
//  combo[0] is position for disk[0], the disk furthest from knob
typedef float combo[];

// how many disks are in combo lock
// front most disk, attached to face, is at index (disks - 1)
// backmost disk, furthest from face, is at index 0
typedef const int disks;

// whether a disk spins up or down
enum spin{up=1, down=-1};
// TODO: figure out how to typedef an array of enums
// currently getting this error
//  error: conflicting declaration 'typedef enum spin spin []'
// typedef spin spin[];
typedef spin spin;

// how many spots on the face
typedef const float spots;

// number of spots to spin from one spot to another on face
typedef float span;

// a specific spot on the face
typedef const float spot;

// difference between consecutive acceptable spots to try on disk
typedef const float gap[];

// size of nub on disk that engages next disk
//   note: assuming top and bottom nub on disk are same size
typedef const float nub[];

// list of actions to take on the face
typedef float act[];


void play_melody(const int melody[],
                 const int note_durations[],
                 const int notes_length,
                 const int speaker_pin){
  // iterate over the notes of the melody:
  for (int this_note = 0; this_note < notes_length; this_note++){

    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    const int note_duration = 1000 / note_durations[this_note];
    tone(speaker_pin, melody[this_note], note_duration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    const int pause_between_notes = note_duration * 1.20;
    delay(pause_between_notes);
    // stop the tone playing:
    noTone(speaker_pin);}}

void play_winning_song(const int speaker_pin){
  // int winning_notes[] =
  //   {NOTE_C5, NOTE_C5, NOTE_C5, NOTE_C5,
  //    NOTE_D5, NOTE_C5, NOTE_D5, NOTE_E5};
  // int winning_durations[] = {4, 16, 8, 8, 4, 4, 4, 2};

  const int winning_notes[] = {NOTE_C5, NOTE_C5, NOTE_C5, NOTE_G5};
  const int winning_durations[] = {4, 16, 8, 2};
  const int notes_length = 4;

  play_melody(winning_notes, winning_durations, notes_length, speaker_pin);}

void play_losing_song(const int speaker_pin){
  const int notes[] = {NOTE_G5, NOTE_C5};
  const int durations[] = {2, 2};
  const int notes_length = 2;

  play_melody(notes, durations, notes_length, speaker_pin);}

void play_starting_song(const int speaker_pin){
  const int notes[] = {NOTE_C5, NOTE_G5, NOTE_E5};
  const int durations[] = {4, 2, 4};
  const int notes_length = 3;

  play_melody(notes, durations, notes_length, speaker_pin);}

void print_indent(){
  Serial.println();
  for(int level = 0; level < indent * 2; level++){
    Serial.print(" ");}}

void debug_print_end_function(boolean d=debug){
  if(d){
    Serial.print("}");
    indent--;}}

/* skip_detect_debug = true; */
/* for(int count = 0; count < count_per_step + 1; count++){ */
/*   for(int steps = 0; steps < count_step_radius; steps++){ */
/*     skip_detect(count, steps);}} */
boolean skip_detect(int32_t count, long steps){
  if(debug && skip_detect_debug){
    print_indent();
    Serial.print("skip_detect(count = ");
    Serial.print(count);
    Serial.print(", steps = ");
    Serial.print(steps);
    Serial.print("){");
    indent++;}

  int32_t count_from_steps = steps * count_per_step;
  int32_t count_difference = count - count_from_steps;
  int32_t count_distance = abs(count_difference);

  boolean skipness = count_distance > count_per_step * 2;
  if(skip_detect_debug){
    print_indent();
    Serial.print("count_from_steps = "); Serial.print(count_from_steps);
    Serial.print(", count_difference = "); Serial.print(count_difference);
    Serial.print(", count_distance = "); Serial.print(count_distance);
    Serial.print(", skipness = "); Serial.print(skipness);}

  if(skipness){
    Serial.println();
    Serial.print("skip");}
  debug_print_end_function(skip_detect_debug);
  return skipness;}

float mod(spot a, spots b){
  // if b is 0, then mod is undefined
  if(b == 0){ return -1;}
  // make sure b is positive
  if(b < 0){ return mod(a, -b);}
  // once b is positive, make sure a is 0 or more
  if(a < 0){ return mod(a + b, b);}
  // once (0 <= a && 0 < b), make sure a is less than b
  if(b <= a){ return mod(a - b, b);}
  // once (b != 0 && (0 <= a < abs(b))), return a
  else{ return a;}}

/* go(0); // dial moves 0 spots */
/* go(numbers_on_dial / 4); // quarter turn towards higher numbers */
/* go(numbers_on_dial); // full turn towards higher numbers */
/* go(numbers_on_dial * 3); // 3 full turns towards higher numbers */
void go(float numbers){
  if(go_debug){
    print_indent();
    Serial.print("go(numbers = ");
    Serial.print(numbers);
    Serial.print("){");
    indent++;}
  float initial_steps = knob.currentPosition();
  float steps_to_go = numbers * steps_per_number;
  float target_steps = initial_steps + steps_to_go;
  float initial_count = encoder.read();
  float target_count = initial_count + steps_to_go * count_per_step;
  if(go_debug){
    print_indent();
    Serial.print("initial_steps = "); Serial.print(initial_steps);
    print_indent();
    Serial.print("steps_to_go = "); Serial.print(steps_to_go);
    print_indent();
    Serial.print("target_steps = "); Serial.print(target_steps);
    print_indent();
    Serial.print("initial_count = "); Serial.print(initial_count);
    print_indent();
    Serial.print("target_count = "); Serial.print(target_count);}
  knob.moveTo(target_steps);
  while(knob.run()){
    int32_t current_count = encoder.read();
    long current_steps = knob.currentPosition();
    if(skip_detect(current_count, current_steps)){
      current_count = encoder.read();
      current_steps = knob.currentPosition();
      long current_steps_from_count = current_count / count_per_step;
      if(go_debug){
        print_indent();
        Serial.print("current_count = "); Serial.print(current_count);
        print_indent();
        Serial.print("current_steps = "); Serial.print(current_steps);
        print_indent();
        Serial.print("current_steps_from_count = ");
        Serial.print(current_steps_from_count);}
      move_shackle(servo,
                   servo_pulse_pin,
                   shackle_rest_position,
                   shackle_rest_time);
      knob.setCurrentPosition(current_steps_from_count);
      knob.moveTo(target_steps);
      delay(1);}}
  debug_print_end_function(go_debug);}

/*
  up
  distance(0, 0, up, 5); // 0
  distance(0, 1, up, 5); // 1
  distance(1, 0, up, 5); // 4
  distance(0, 0.5, up, 5); // 0.50
  distance(0.5, 0, up, 5); // 4.50

  down
  distance(0, 0, down, 5); // 0
  distance(0, 1, down, 5); // 4
  distance(1, 0, down, 5); // 1
  distance(0, 0.5, down, 5); // 4.50
  distance(0.5, 0, down, 5); // 0.50
*/
span distance(spot from, spot to,
              spin spin, spots spots){
  if(spin == up){
    return mod(to - from, spots);}
  else{
    return mod(from - to, spots);}}

void print_gap(gap gap, disks disks){
  for(int index = 0; index < disks; index++){
    Serial.print(!index?"[":" ");
    if(abs(gap[index]) < 10){Serial.print(" ");}
    // align positives and negatives, which have a leading '-' sign
    if(0 <= gap[index]){Serial.print(" ");}
    Serial.print(gap[index]);}
  Serial.print("]");}

void print_spin(spin spin[], disks disks){
  //TODO: figure out how to call print_gap instead
  for(int index = 0; index < disks; index++){
    Serial.print(!index?"[":" ");
    if(abs(spin[index]) < 10){Serial.print(" ");}
    // align positives and negatives, which have a leading '-' sign
    if(0 <= spin[index]){Serial.print(" ");}
    Serial.print(spin[index]);}
  Serial.print("]");}

void print_combo(combo combo, disks disks){
  print_gap(combo, disks);}

void print_nub(nub nub, disks disks){
  print_gap(nub, disks);}

void print_act(act act, disks disks){
  print_gap(act, disks);}

// is a <= b <= c
boolean outside(spot a, spot lo, spot hi,
                spin spin,
                spots spots){
  return
    distance(lo, hi, spin, spots)
    <
    distance(lo, a, spin, spots);}

void copy_combo(combo from, combo to, disks disks){
  for(int index = 0; index < disks; index++){
    to[index] = from[index];}}


/*
  TODO: combine these 3 functions into a single function
  can_move_just_last_digit, can_move_just_last_2_digits,
  and need_to_move_all_3_digits
 */
boolean can_move_just_last_digit(combo current,
                                 spin spin[], gap gap, nub nub,
                                 spots spots, disks disks){
  if(false&&debug){
    Serial.println();
    Serial.print("can move just last digit(current");
    print_combo(current, disks);
    Serial.print(" spin");
    print_spin(spin, disks);
    Serial.print(" gap");
    print_gap(gap, disks);
    Serial.print(" nub");
    print_nub(nub, disks);
    Serial.print(" spots");
    Serial.print((int)spots);
    Serial.print(" disks");
    Serial.print((int)disks);
    Serial.print(")");}

  int bigger = gap[2] < nub[2] ? nub[2] : gap[2];
  spot furthest_covered = mod(current[2] + spin[2] * bigger, spots);
  return outside(current[1], current[2], furthest_covered,
                 spin[2], spots);}

boolean can_move_just_last_2_digits(combo current,
                                    spin spin[], gap gap, nub nub,
                                    spots spots, disks disks){
  if(false&&debug){
    Serial.println();
    Serial.print("can move just last 2 digits(current");
    print_combo(current, disks);
    Serial.print(" spin");
    print_spin(spin, disks);
    Serial.print(" gap");
    print_gap(gap, disks);
    Serial.print(" nub");
    print_nub(nub, disks);
    Serial.print(" spots");
    Serial.print((int)spots);
    Serial.print(" disks");
    Serial.print((int)disks);}

  int bigger = gap[1] < nub[1] ? nub[1] : gap[1];
  spot furthest_covered = mod(current[1] + spin[1] * bigger, spots);
  return outside(current[0], current[1], furthest_covered, spin[1], spots);}

boolean need_to_move_all_3_digits(combo current,
                                  spin spin[], gap gap, nub nub,
                                  spots spots, disks disks){
  if(false&&debug){
    Serial.println();
    Serial.print("need to move all 3 digits(current");
    print_combo(current, disks);
    Serial.print(" spin");
    print_spin(spin, disks);
    Serial.print(" gap");
    print_gap(gap, disks);
    Serial.print(" nub");
    print_nub(nub, disks);
    Serial.print(" spots");
    Serial.print((int)spots);
    Serial.print(" disks");
    Serial.print((int)disks);}

  return true;}

// calculate next combo given current combo,
// and gap sizes for each disk
// TODO: investigate using recursion with
//   3 cases, everything before the top-most disk that needs to move,
//   the top-most disk that needs to move
//   and everything after the topmost disk that needs to move.
//
//   this would cut down on the conceptual repetition below
void next_combo(combo current, spin spin[], gap gap, nub nub,
                spots spots, disks disks,
                combo next){
  if(false&&debug){
    Serial.println();
    Serial.print("next_combo(current");
    print_combo(current, disks);
    Serial.print(" spin");
    print_spin(spin, disks);
    Serial.print(" gap");
    print_gap(gap, disks);
    Serial.print(" nub");
    print_nub(nub, disks);
    Serial.print(" spots");
    Serial.print((int)spots);
    Serial.print(" disks");
    Serial.print((int)disks);
    Serial.print(" next");
    print_combo(next, disks);
    Serial.print(")");}

  int delta0, delta1, delta2;

  if(can_move_just_last_digit(current, spin, gap, nub, spots, disks)){
    delta0 = 0;
    delta1 = 0;
    delta2 = spin[2] * gap[2];

    next[0] = mod(current[0] + delta0, spots);
    next[1] = mod(current[1] + delta1, spots);
    next[2] = mod(current[2] + delta2, spots);}
  else if(can_move_just_last_2_digits(current, spin, gap, nub,
                                      spots, disks)){
    delta0 = 0;
    delta1 = spin[1] * gap[1];
    delta2 = spin[2] * nub[2];

    next[0] = mod(current[0] + delta0, spots);
    next[1] = mod(current[1] + delta1, spots);
    next[2] = mod(next[1]    + delta2, spots);}
  else if(need_to_move_all_3_digits(current, spin, gap, nub,
                                    spots, disks)){
    delta0 = spin[0] * gap[0];
    delta1 = spin[1] * nub[1];
    delta2 = spin[2] * nub[2];

    next[0] = mod(current[0] + delta0, spots);
    next[1] = mod(next[0]    + delta1, spots);
    next[2] = mod(next[1]    + delta2, spots);}}

boolean combos_equal(combo a, combo b, disks disks){
  for(int index = 0; index < disks; index++){
    if(a[index] != b[index]){return false;}}
  return true;}

/* initially, disks are in unknown positions,
   except front most dial face disk is at spot.
   from that state, ensure that disks are at spots given in combo */
void enter_combo(spot spot, combo combo,
                 spin spin[], disks disks, spots spots){
  if(debug){
    Serial.println();
    Serial.print("spot = ");Serial.print(spot);
    Serial.print(", combo = ");
    print_combo(combo, disks);}

  if(slow){delay(slow_spot_delay);}
  go(spin[0] * (2 * spots + distance(spot, combo[0], spin[0], spots)));
  if(slow){delay(slow_spot_delay);}
  go(spin[1] * (spots + distance(combo[0], combo[1], spin[1], spots)));
  if(slow){delay(slow_spot_delay);}
  go(spin[2] * distance(combo[1], combo[2], spin[2], spots));}

// calculate how much to rotate to get from one combo
//  to another
//TODO: calculate this with disks instead of hard coding 3
void calculate_acts(combo from, combo to,
                    nub nub, gap gap, spin spin[],
                    spots spots,
                    act act){
  if(from[0] == to[0]
     && from[1] == to[1]
     && from[2] != to[2]){
    act[0] = 0;
    act[1] = 0;
    act[2] = spin[2] * gap[2];}
  else if(from[0] == to[0]
          && from[1] != to[1]){
    act[0] = 0;
    act[1] = spin[1] * (gap[1]
                        + distance(from[2], from[1], spin[1], spots));
    act[2] = spin[2] * nub[2];}
  else if(from[0] != to[0]){
    act[0] = spin[0] * (gap[0]
                        + distance(from[2], from[1], spin[0], spots)
                        + distance(from[1], from[0], spin[2], spots));
    act[1] = spin[1] * (nub[1] + spots);
    act[2] = spin[2] * nub[2];}}

void clear_act(act act, disks disks){
  for(int index = 0; index < disks; index++){
    act[index] = 0;}}

int calculate_combo_count(disks disks, spots spots, gap gap, nub nub){
  int product = ceil(spots / gap[0]);
  for(int index = 1; index < disks; index++){
    float coefficient =
      ceil((spots - 2 * nub[index])/(float)gap[index] + 1);
    product *= coefficient;}
  return product;}

float get_spot_from_user(Keypad keypad){
  float spot = 0;
  char key;
  Serial.println();
  Serial.print("what spot is the dial at? ");
  const int spot_digits = 4;
  for(int index = 0; index < spot_digits; index++){
    while(!(key = keypad.getKey()));
    spot = spot * 10 + (key - '0');
    Serial.println(spot / 100);}
  spot /= 100;
  Serial.print(spot);
  return spot;}

int feedback_to_pulse(int feedback){
  // check for feedback at the beginning
  if(feedback < (servo_pulse_feedback[0][1]
                 + servo_pulse_feedback[1][1]) / 2){
    return servo_pulse_feedback[0][0];}

  // check for feedbacks in the middle
  for(int index = 1; index < pulse_top_index; index++){
    int prior_row_feedback = servo_pulse_feedback[index - 1][1];
    int next_row_feedback = servo_pulse_feedback[index + 1][1];
    int current_row_feedback = servo_pulse_feedback[index][1];

    int range_bottom = (prior_row_feedback + current_row_feedback) / 2;
    int range_top = (current_row_feedback + next_row_feedback) / 2;

    if( range_bottom <= feedback && feedback < range_top){
      return servo_pulse_feedback[index][0];}}

  // feedback must be at the end if code gets here
  return servo_pulse_feedback[pulse_top_index][0];}

int get_servo_feedback(const int feedback_pin,
                       const int feedback_settle_time){
  delay(feedback_settle_time);
  return analogRead(feedback_pin);}

boolean is_shackle_down(const int servo_feedback_pin,
                        const int feedback_settle_time){
  const int servo_horn_pulse =
    feedback_to_pulse(get_servo_feedback(servo_feedback_pin,
                                         feedback_settle_time));
  if(debug || shackle_down_pulse <= servo_horn_pulse){
    Serial.println();
    Serial.print("current servo horn pulse = ");
    Serial.print(servo_horn_pulse);}
  return servo_horn_pulse <= shackle_down_pulse;}

void perform_act(act act, disks disks){
  for(int index = 0; index < disks; index++){
    int action = act[index];
    if(slow && action != 0){delay(slow_spot_delay);}
    go(action);}}

void move_shackle(Servo servo,
                  const int servo_pulse_pin,
                  const int target_pulse,
                  const int move_time){
  servo.attach(servo_pulse_pin);
  servo.write(target_pulse);
  delay(move_time);
  servo.detach();}

boolean combo_cracked(const int servo_feedback_pin,
                      const int feedback_settle_time){
  return !is_shackle_down(servo_feedback_pin, feedback_settle_time);}

void finish(const boolean win,
            const int speaker_pin,
            spot spot,
            combo combo,
            spin spin[],
            disks disks,
            spots spots){
  if (win) {
    const int slow_knob_speed = 50,
      slow_knob_acceleration = 2500;
    slow = true;
    knob.setAcceleration(slow_knob_acceleration);
    knob.setMaxSpeed(slow_knob_speed);
    while(true){
      while(!is_shackle_down(servo_feedback_pin, feedback_settle_time)){
        Serial.println();
        Serial.print("lock the lock to continue");
        delay(500);}
      enter_combo(spot, combo, spin, disks, spots);

      move_shackle(servo,
                   servo_pulse_pin,
                   shackle_open_position,
                   shackle_open_time);
      play_winning_song(speaker_pin);}}
  else{
    play_losing_song(speaker_pin);
    knob.disableOutputs();
    servo.detach();
    Serial.flush();
    exit(0);}}

void demo(){
  combo combo = {36, 26, 16};
  spin spin[] = {down, up, down};
  disks disks = 3;
  spots spots = 40;
  enter_combo(0, combo, spin, disks, spots);
  servo.attach(servo_pulse_pin);
  servo.write(180);
  delay(1000);
  finish(true,
         speaker_pin,
         combo[disks - 1],
         combo,
         spin,
         disks,
         spots);}

void setup(){
  Serial.begin(9600);
  while(!Serial);
  knob.setMaxSpeed(knob_speed);
  knob.setAcceleration(knob_acceleration);
  pinMode(servo_pulse_pin, OUTPUT);
  pinMode(servo_feedback_pin, INPUT);
  pinMode(speaker_pin, OUTPUT);

  play_starting_song(speaker_pin);
  go(-get_spot_from_user(keypad));}

void loop(){
  break_combo();}

void break_combo(){
  boolean win = false;
  disks disks = 3;
  spots spots = 40;

  combo current = {0, 34, 0};
  gap gap = {1, 4, 4};
  spin spin[] = {down, up, down};
  nub nub = {0, 6, 6};
  combo next = {0, 0, 0};
  act act = {0, 0, 0};
  const int combo_count =
    calculate_combo_count(disks, spots, gap, nub);

  while(!is_shackle_down(servo_feedback_pin, feedback_settle_time)){
    Serial.println();
    Serial.print("lock the lock to start");
    delay(500);}

  enter_combo(0, current, spin, disks, spots);

  for(int index = 0; index < combo_count; index++){
    next_combo(current, spin, gap, nub, spots, disks, next);
    calculate_acts(current, next, nub, gap, spin, spots, act);

    char index_string[6];
    Serial.println();
    snprintf(index_string, sizeof(index_string), "% 5d", index);
    Serial.print(index_string);
    Serial.print(" ");
    print_combo(current, disks);
    Serial.print(" + ");
    print_act(act, disks);
    Serial.print(" = ");
    print_combo(next, disks);

    copy_combo(next, current, disks);

    perform_act(act, disks);

    move_shackle(servo,
                 servo_pulse_pin,
                 shackle_open_position,
                 shackle_open_time);

    if(combo_cracked(servo_feedback_pin, feedback_settle_time)){
      win = true;
      Serial.println();
      Serial.print("winner winner, chicken dinner: ");
      print_combo(current, disks);
      break;}}

  finish(win,
         speaker_pin,
         current[disks - 1],
         current,
         spin,
         disks,
         spots);}

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

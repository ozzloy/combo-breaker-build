/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

const int encoder_signal_a_pin = 2;
const int encoder_signal_b_pin = 3;
const int direction_pin = 4;
const int step_pin = 5;
const int speaker_pin = 8;
const int servo_pulse_pin = 9;
const int servo_feedback_pin = A0;

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

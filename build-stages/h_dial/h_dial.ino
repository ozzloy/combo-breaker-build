/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

#include <AccelStepper.h>

#define baudrate 9600

const int motor_interface_type = 1;
const int step_pin = 5;
const int dir_pin = 4;
const AccelStepper dial =
  AccelStepper(motor_interface_type, step_pin, dir_pin);

const int encoder_signal_a_pin = 2;
const int encoder_signal_b_pin = 3;

const int steps_per_rotation = 200;
const int microsteps_per_step = 1;
const int stepper_driver_delay_microseconds = 10000;

#include <Encoder.h>

Encoder encoder(encoder_signal_a_pin, encoder_signal_b_pin);
const int encoder_one_rotation_count = 1200;

const int numbers_on_dial = 40;

void on(int pin){
  digitalWrite(pin, HIGH);}

void off(int pin){
  digitalWrite(pin, LOW);}

void do_microstep(){
  on(step_pin);
  delayMicroseconds(stepper_driver_delay_microseconds);
  off(step_pin);
  delayMicroseconds(stepper_driver_delay_microseconds);}

void do_step(){
  for(int microstep = 0; microstep < microsteps_per_step; microstep++){
    do_microstep();}}

void rotate_dial_once(){
  uint32_t start = encoder.read();
  while((encoder.read() - start) < encoder_one_rotation_count){
    do_step();}}

void spin_once(){
  encoder.write(0);
  int32_t encoder_position;
  while((encoder_position = encoder.read())
        < encoder_one_rotation_count - 1){
    int steps_left =
      (encoder_one_rotation_count - encoder_position)
      * steps_per_rotation
      / encoder_one_rotation_count;
    Serial.print("steps left = ");
    Serial.println(steps_left);
    dial.setCurrentPosition(0);
    dial.runToNewPosition(steps_left);}}

void setup(){
  Serial.begin(baudrate);
  dial.setMaxSpeed(1000);
  dial.setAcceleration(200*200);
  dial.setSpeed(500);
  encoder.write(0);
  dial.setCurrentPosition(0);
  for(int count_down = 10; count_down > 10; count_down--){
    Serial.println(count_down);Serial.flush();}
  Serial.println("starting");Serial.flush();}

void loop(){
  Serial.print("begin ");
  Serial.print(encoder.read());
  Serial.print(", ");
  Serial.flush();
  spin_once();
  delay(1000);
  Serial.print("end ");
  Serial.print(encoder.read());
  Serial.println();
  Serial.flush();
  delay(1000);}

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

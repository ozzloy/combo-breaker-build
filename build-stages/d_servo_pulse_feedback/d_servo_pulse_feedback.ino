/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */
/*
  sweep through positions and record feedback at the position.
  print out c code to create a lookup table in a variable.

* output

#+BEGIN_SRC c
int servo_pulse_feedback[servo_rows][servo_columns] =
  {{  0, 117},
   {  5, 128},
   { 10, 145},
   { 15, 159},
   { 20, 168},
   { 25, 178},
   { 30, 190},
   { 35, 200},
   { 40, 211},
   { 45, 220},
   { 50, 230},
   { 55, 244},
   { 60, 252},
   { 65, 266},
   { 70, 273},
   { 75, 287},
   { 80, 296},
   { 85, 307},
   { 90, 317},
   { 95, 325},
   {100, 338},
   {105, 347},
   {110, 358},
   {115, 368},
   {120, 381},
   {125, 390},
   {130, 400},
   {135, 408},
   {140, 418},
   {145, 430},
   {150, 438},
   {155, 448},
   {160, 457},
   {165, 468},
   {170, 478},
   {175, 486},
   {180, 499}};
#+END_SRC
*/

#include <Servo.h>
#include "dss_m15s.h"

// data flow
#define BAUDRATE 9600

#define SERVO_PULSE_PIN 9
#define SERVO_FEEDBACK_PIN A0

#define PULSE_STEP 5
#define PULSE_BOTTOM 0

#define PULSE_RANGE (PULSE_TOP - PULSE_BOTTOM)
#define PULSE_TOP_INDEX (PULSE_RANGE / PULSE_STEP)
#define servo_rows (PULSE_RANGE / PULSE_STEP + 1)
#define servo_columns 2

Servo servo;

int servo_pulse_feedback[servo_rows][servo_columns];

// utility function to grab measured feedback given a pulse
void setup(){
  Serial.begin(BAUDRATE);
  pinMode(SERVO_FEEDBACK_PIN, INPUT);
  servo.attach(SERVO_PULSE_PIN);

  servo.write(0);
  delay(2000);
  servo.detach();}

void sweep_pulses_record_feedbacks(){
  int servo_pulse;
  int servo_feedback;

  for(int index = 0;
      index <= PULSE_TOP_INDEX;
      index++){
    // go to location
    servo_pulse = index * PULSE_STEP + PULSE_BOTTOM;
    servo.attach(SERVO_PULSE_PIN);
    servo.write(servo_pulse);
    /* a more sophisticated algorithm is needed for generic moving
       from any location to any location.

       in this specific application, servo is just sweeping from
       one extreme to the other linearly.  waiting for a fixed amount
       of time seems reasonable.  from experiment, it has been observed
       to work pretty well. */
    delay(SERVO_MOVE_TIME);
    servo.detach();
    delay(FEEDBACK_SETTLE_TIME);
    if(index == PULSE_TOP_INDEX){
      delay(250);}
    // record pulse and feedback value
    servo_feedback = analogRead(SERVO_FEEDBACK_PIN);
    servo_pulse_feedback[index][0] = servo_pulse;
    servo_pulse_feedback[index][1] = servo_feedback;}}

void print_pulse_feedback(int servo_pulse_feedback[][servo_columns],
                          int rows,
                          int columns){
  int pulse, feedback;
  Serial.println("int servo_pulse_feedback[servo_rows][servo_columns] =");
  Serial.print("  {");

  for(int row = 0; row < rows; row++){
    pulse = servo_pulse_feedback[row][0];
    feedback = servo_pulse_feedback[row][1];
    Serial.print(row == 0 ? "" : "   ");
    Serial.print("{");
    if(pulse < 10){
      Serial.print("  ");}
    else if(pulse < 100){
      Serial.print(" ");}
    Serial.print(pulse);
    Serial.print(", ");
    Serial.print(feedback);
    Serial.print("}");
    Serial.print(row < PULSE_TOP_INDEX ? ",\n" : "");}
  Serial.println("};");}

void loop(){
  sweep_pulses_record_feedbacks();
  print_pulse_feedback(servo_pulse_feedback,
                       servo_rows,
                       servo_columns);
  Serial.flush();

  servo.attach(SERVO_PULSE_PIN);
  servo.write(85);
  delay(2000);
  servo.detach();
  exit(0);}

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

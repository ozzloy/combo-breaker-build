/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

#define FEEDBACK_SETTLE_TIME 1000
//#define FEEDBACK_SETTLE_TIME 25
#define PULSE_TOP 180

#define SERVO_MOVE_TIME 1000

/*

int servo_pulse_feedback[servo_rows][servo_columns] =
  {{  0, 88},
   {  5, 104},
   { 10, 124},
   { 15, 141},
   { 20, 160},
   { 25, 178},
   { 30, 197},
   { 35, 215},
   { 40, 234},
   { 45, 252},
   { 50, 271},
   { 55, 290},
   { 60, 309},
   { 65, 327},
   { 70, 346},
   { 75, 364},
   { 80, 383},
   { 85, 402},
   { 90, 420},
   { 95, 439},
   {100, 457},
   {105, 476},
   {110, 494},
   {115, 514},
   {120, 533},
   {125, 549},
   {130, 568},
   {135, 588},
   {140, 606},
   {145, 625},
   {150, 642},
   {155, 661},
   {160, 680},
   {165, 700},
   {170, 716},
   {175, 736},
   {180, 753}};
int servo_pulse_feedback[servo_rows][servo_columns] =
  {{  0, 90},
   {  5, 105},
   { 10, 124},
   { 15, 142},
   { 20, 160},
   { 25, 178},
   { 30, 199},
   { 35, 215},
   { 40, 235},
   { 45, 253},
   { 50, 272},
   { 55, 290},
   { 60, 309},
   { 65, 328},
   { 70, 346},
   { 75, 366},
   { 80, 384},
   { 85, 402},
   { 90, 420},
   { 95, 439},
   {100, 458},
   {105, 476},
   {110, 495},
   {115, 513},
   {120, 531},
   {125, 549},
   {130, 568},
   {135, 588},
   {140, 606},
   {145, 626},
   {150, 643},
   {155, 662},
   {160, 680},
   {165, 700},
   {170, 716},
   {175, 735},
   {180, 753}};
int servo_pulse_feedback[servo_rows][servo_columns] =
  {{  0, 91},
   {  5, 107},
   { 10, 126},
   { 15, 143},
   { 20, 162},
   { 25, 180},
   { 30, 199},
   { 35, 217},
   { 40, 237},
   { 45, 255},
   { 50, 273},
   { 55, 292},
   { 60, 311},
   { 65, 329},
   { 70, 347},
   { 75, 366},
   { 80, 386},
   { 85, 404},
   { 90, 422},
   { 95, 440},
   {100, 459},
   {105, 478},
   {110, 496},
   {115, 515},
   {120, 532},
   {125, 552},
   {130, 570},
   {135, 590},
   {140, 608},
   {145, 627},
   {150, 644},
   {155, 663},
   {160, 682},
   {165, 702},
   {170, 718},
   {175, 737},
   {180, 757}};
int servo_pulse_feedback[servo_rows][servo_columns] =
  {{  0, 75},
   {  5, 87},
   { 10, 103},
   { 15, 118},
   { 20, 134},
   { 25, 149},
   { 30, 166},
   { 35, 180},
   { 40, 197},
   { 45, 212},
   { 50, 227},
   { 55, 243},
   { 60, 258},
   { 65, 274},
   { 70, 290},
   { 75, 305},
   { 80, 322},
   { 85, 338},
   { 90, 352},
   { 95, 369},
   {100, 384},
   {105, 399},
   {110, 415},
   {115, 430},
   {120, 446},
   {125, 461},
   {130, 478},
   {135, 493},
   {140, 508},
   {145, 526},
   {150, 538},
   {155, 556},
   {160, 570},
   {165, 585},
   {170, 600},
   {175, 618},
   {180, 634}};

 */
/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

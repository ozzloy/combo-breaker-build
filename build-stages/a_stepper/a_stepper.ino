/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

#define CW true // clockwise
#define CCW (!CW) // counter-clockwise

// direction pin by 4 steps,
//   checking intervals for the last number of the lock
#define DIR_PIN 4
// steps by 5 with the 2nd number of the lock
#define STEP_PIN 5

// rotates the entire knob 360 degrees
#define ONE_SHAFT_ROTATION 200
// shifts the knob by a bit.
#define MICROSTEPS 8

// #define MIN_DELAY (1600 / MICROSTEPS) // was 200
#define MIN_DELAY 400 // Delays every knob turn or step.

// data flow
#define BAUDRATE 9600

void on(int pin){ digitalWrite(pin, HIGH);}
void off(int pin){ digitalWrite(pin, LOW);}

void step(int steps, boolean cw){
  cw ? on(DIR_PIN) : off(DIR_PIN);
  for(int i = 0; i < steps * MICROSTEPS; i++){
    off(STEP_PIN);
    delayMicroseconds(MIN_DELAY);
    on(STEP_PIN);
    delayMicroseconds(MIN_DELAY);}}

void setup(){
  Serial.begin(BAUDRATE);
  pinMode(DIR_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);}

void loop(){
  Serial.println("clockwise");
  step(ONE_SHAFT_ROTATION, CW);
  delay(2000);
  Serial.println("1/2 counterclockwise");
  step(ONE_SHAFT_ROTATION / 2, CCW);
  delay(1000);
  Serial.println("1/2 counterclockwise");
  step(ONE_SHAFT_ROTATION / 2, CCW);
  delay(1000);}

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

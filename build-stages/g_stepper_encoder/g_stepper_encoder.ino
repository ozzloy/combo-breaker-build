/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

#define baudrate 9600

const int step_pin = 5;
const int dir_pin = 4;
const int encoder_signal_a_pin = 2;
const int encoder_signal_b_pin = 3;

const int steps_per_rotation = 200;
const int microsteps_per_step = 1;
const int stepper_driver_delay_microseconds = 400;

#include <Encoder.h>

Encoder encoder(encoder_signal_a_pin, encoder_signal_b_pin);

void on(int pin){
  digitalWrite(pin, HIGH);}

void off(int pin){
  digitalWrite(pin, LOW);}

void do_microstep(){
  on(step_pin);
  delayMicroseconds(stepper_driver_delay_microseconds);
  off(step_pin);
  delayMicroseconds(stepper_driver_delay_microseconds);}

void do_step(){
  for(int microstep = 0; microstep < microsteps_per_step; microstep++){
    do_microstep();}}

void setup(){
  pinMode(dir_pin, OUTPUT);
  pinMode(step_pin, OUTPUT);
  on(dir_pin);
  Serial.begin(baudrate);}

void print_one_rotation_count(){
  int start = encoder.read();
  for(int step = 0; step < steps_per_rotation; step++){
    do_step();}
  // let shaft settle
  delay(50);
  int end = encoder.read();
  Serial.print("const int encoder_one_rotation_count = ");
  Serial.print(end - start);
  Serial.println(";");
  Serial.flush();}

void loop(){
  for(int index = 0; index < 10; index++){
    print_one_rotation_count();}
  exit(0);}

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

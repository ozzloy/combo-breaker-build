#include <Encoder.h>

#define CW true // clockwise
#define CCW (!CW) // counter-clockwise

// 2nd number for the combination
#define ENCODER1 2
// 3rd number for the combination
#define ENCODER2 3

// direction pin by 4 steps,
//   checking intervals for the last number of the lock
#define DIR_PIN 4
// steps by 5 with the 2nd number of the lock
#define STEP_PIN 5

// rotates the entire knob 360 degrees
#define ONE_SHAFT_ROTATION 200
// shifts the knob by a bit
#define MICROSTEPS 8

// how long it takes for the dial to stop after a movement,
//
// determined experimentally by watching serial monitor, confirming that
// dial encoder reads the same positions when it's supposed to
//
// waits after moving the lock knob
#define DIAL_SETTLE_TIME 25

Encoder dial_encoder(ENCODER1, ENCODER2);
long dial_position;

// #define MIN_DELAY (1600 / MICROSTEPS) // was 200
// delays every knob turn or step
#define MIN_DELAY 400

// data flow
#define BAUDRATE 9600

void on(int pin){ digitalWrite(pin, HIGH);}
void off(int pin){ digitalWrite(pin, LOW);}

void step(int steps, boolean cw){
  cw ? on(DIR_PIN) : off(DIR_PIN);
  for(int i = 0; i < steps * MICROSTEPS; i++){
    on(STEP_PIN);
    delayMicroseconds(MIN_DELAY);
    off(STEP_PIN);
    delayMicroseconds(MIN_DELAY);}}

void setup(){
  Serial.begin(BAUDRATE);
  dial_position = dial_encoder.read();
  pinMode(DIR_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);}

void loop(){
  /*
    wait 2 seconds
    spin one full turn clockwise,
    wait 1 second
    spin 1/2 turn counterclockwise
  */

  delay(2000);

  delay(DIAL_SETTLE_TIME);
  dial_position = dial_encoder.read();
  Serial.println("\tclockwise, starting from dial position: ");
  Serial.println(dial_position);
  step(ONE_SHAFT_ROTATION, CW);
  delay(DIAL_SETTLE_TIME);
  dial_position = dial_encoder.read();
  Serial.println("\tfinished at dial position: ");
  Serial.println(dial_position);

  delay(1000);

  delay(DIAL_SETTLE_TIME);
  dial_position = dial_encoder.read();
  Serial.println("\t1/2 counterclockwise, starting from dial position: ");
  Serial.println(dial_position);
  step(ONE_SHAFT_ROTATION / 2, CCW);
  delay(DIAL_SETTLE_TIME);
  dial_position = dial_encoder.read();
  Serial.println("\tfinished at dial position: ");
  Serial.println(dial_position);}

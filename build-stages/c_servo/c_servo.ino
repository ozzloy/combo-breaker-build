/*
  example output:
int position_feedback[180/5 + 1][2] =
  {
  {0, 129},
  {5, 135},
  {10, 148},
  {15, 160},
  {20, 171},
  {25, 180},
  {30, 193},
  {35, 202},
  {40, 213},
  {45, 223},
  {50, 236},
  {55, 245},
  {60, 257},
  {65, 268},
  {70, 276},
  {75, 289},
  {80, 298},
  {85, 309},
  {90, 321},
  {95, 329},
  {100, 343},
  {105, 350},
  {110, 360},
  {115, 373},
  {120, 382},
  {125, 393},
  {130, 402},
  {135, 413},
  {140, 422},
  {145, 433},
  {150, 444},
  {155, 454},
  {160, 462},
  {165, 474},
  {170, 481},
  {175, 493},
  {180, 502}
  };
 */

#include <Encoder.h>
#include <Servo.h>

// clockwise
#define CW true
// counter-clockwise
#define CCW (!CW)

// 2nd number for the combination
#define ENCODER1 2
// 3rd number for the combination
#define ENCODER2 3

// direction pin by 4 steps,
//   checking intervals for the last number of the lock
#define DIR_PIN 4
// steps by 5 with the 2nd number of the lock
#define STEP_PIN 5

// sets up for the arm connecting to the shackle
#define SERVO_PIN 9
#define SERVO_FEEDBACK_PIN A2

// rotates the entire knob 360 degrees
#define ONE_SHAFT_ROTATION 200
// shifts the knob by a bit
#define MICROSTEPS 8

// waits after moving the shackle
#define SHACKLE_SETTLE_TIME 1000
// how long it takes for the dial to stop after a movement,
//
// determined experimentally by watching serial monitor, confirming that
// dial encoder reads the same positions when it's supposed to
#define DIAL_SETTLE_TIME 25  // Waits after moving the lock knob

Encoder dial_encoder(ENCODER1, ENCODER2);
long dial_position;

// #define MIN_DELAY (1600 / MICROSTEPS) // was 200
// minimal delay overall
#define MIN_DELAY 400

Servo shackle;

// data flow
#define BAUDRATE 9600

void on(int pin){ digitalWrite(pin, HIGH);}
void off(int pin){ digitalWrite(pin, LOW);}

void step(int steps, boolean cw){
  cw ? on(DIR_PIN) : off(DIR_PIN);
  for(int i = 0; i < steps * MICROSTEPS; i++){
    on(STEP_PIN);
    delayMicroseconds(MIN_DELAY);
    off(STEP_PIN);
    delayMicroseconds(MIN_DELAY);}}

int get_shackle_feedback(){
  return analogRead(SERVO_FEEDBACK_PIN);

  int value;
  int readings_count = 1;
  for(int i = 0; i < readings_count; i++){
    value += analogRead(SERVO_FEEDBACK_PIN);}
  return (int)(value / readings_count);}

void setup(){
  Serial.begin(BAUDRATE);
  dial_position = dial_encoder.read();
  pinMode(DIR_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  pinMode(SERVO_FEEDBACK_PIN, INPUT);
  shackle.attach(SERVO_PIN);}

boolean shackle_has_moved(int prior_feedback){

  return prior_feedback;
}

// analog range: 111 - 467
// position/pulse range: 0 - 180
// servo spins clockwise with increasing position/pulse input
// detach arm from servo and let it spin freely
// save the output of this program in the header of the next program
void loop(){
  shackle.write(0);
  delay(2000);
  Serial.println();
  Serial.println();
  Serial.println("int position_feedback[180/5 + 1][2] =");
  Serial.println("  {");
  for(int i = 0; i < 181; i+=5){
    Serial.print("   {");
    Serial.print(i);
    shackle.write(i);
    delay(SHACKLE_SETTLE_TIME);
    Serial.print(", ");
    Serial.print(analogRead(SERVO_FEEDBACK_PIN));
    Serial.print("}");
    Serial.println(i < 180 ? "," : "");}
  Serial.print("  };");
  Serial.println();
  Serial.println();
  delay(1000);
  shackle.write(0);
  delay(100000);
  exit(0);}

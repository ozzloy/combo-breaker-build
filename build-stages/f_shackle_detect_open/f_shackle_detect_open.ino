/* GNU AGPLv3 (or later at your option)
   see bottom for more license info */

#include <Servo.h>
#define BAUDRATE 9600

#define SERVO_MOVE_TIME 1000
#define FEEDBACK_SETTLE_TIME 25

int minimum_shackle_position = 80,
  maximum_shackle_locked_position = 115,
  maximum_shackle_unlocked_position = 145;
#define SERVO_PULSE_PIN 9
#define SERVO_FEEDBACK_PIN A0

#define PULSE_STEP 5
#define PULSE_BOTTOM 0
#define PULSE_TOP 180

#define PULSE_RANGE (PULSE_TOP - PULSE_BOTTOM)
#define PULSE_TOP_INDEX (PULSE_RANGE / PULSE_STEP)
#define servo_rows (PULSE_RANGE / PULSE_STEP + 1)
#define servo_columns 2

int servo_pulse_feedback[servo_rows][servo_columns] =
  {{  0, 117},
   {  5, 128},
   { 10, 145},
   { 15, 159},
   { 20, 168},
   { 25, 178},
   { 30, 190},
   { 35, 200},
   { 40, 211},
   { 45, 220},
   { 50, 230},
   { 55, 244},
   { 60, 252},
   { 65, 266},
   { 70, 273},
   { 75, 287},
   { 80, 296},
   { 85, 307},
   { 90, 317},
   { 95, 325},
   {100, 338},
   {105, 347},
   {110, 358},
   {115, 368},
   {120, 381},
   {125, 390},
   {130, 400},
   {135, 408},
   {140, 418},
   {145, 430},
   {150, 438},
   {155, 448},
   {160, 457},
   {165, 468},
   {170, 478},
   {175, 486},
   {180, 499}};

Servo servo;

int feedback_to_pulse(int feedback){
  // check for feedback at the beginning
  if(feedback < (servo_pulse_feedback[0][1]
                 + servo_pulse_feedback[1][1]) / 2){
    return servo_pulse_feedback[0][0];}

  // check for feedbacks in the middle
  for(int index = 1; index < servo_rows; index++){
    int prior_row_feedback = servo_pulse_feedback[index - 1][1];
    int next_row_feedback = servo_pulse_feedback[index + 1][1];
    int current_row_feedback = servo_pulse_feedback[index][1];

    int range_bottom = (prior_row_feedback + current_row_feedback) / 2;
    int range_top = (current_row_feedback + next_row_feedback) / 2;

    if( range_bottom <= feedback && feedback < range_top){
      return servo_pulse_feedback[index][0];}}

  // feedback must be at the end if code gets here
  return servo_pulse_feedback[PULSE_TOP_INDEX][0];}

void setup(){
  Serial.begin(BAUDRATE);}

void loop(){
  servo.attach(SERVO_PULSE_PIN);
  servo.write(PULSE_TOP);
  delay(SERVO_MOVE_TIME);
  servo.detach();
  delay(SERVO_MOVE_TIME);
  delay(FEEDBACK_SETTLE_TIME);
  int feedback = analogRead(SERVO_FEEDBACK_PIN);
  int pulse = feedback_to_pulse(feedback);
  Serial.print("shackle is ");
  if (pulse < maximum_shackle_unlocked_position - PULSE_STEP) {
    Serial.print("locked");}
  else{
    Serial.print("unlocked");}
  Serial.println();
  Serial.flush();
  servo.attach(SERVO_PULSE_PIN);
  servo.write(minimum_shackle_position);
  delay(SERVO_MOVE_TIME);
  servo.detach();
  exit(0);}

/*
  This file is part of combo-breaker-build.

  combo-breaker-build is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  combo-breaker-build is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.
*/

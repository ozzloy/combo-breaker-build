#! /usr/bin/env bash

# GNU AGPLv3 (or later at your option)
# see bottom for more license info

mkdir -p mechanical-drawing
cd mechanical-drawing

#https://www.phidgets.com/productfiles/3320/3320_0/Documentation/3320_0_Mechanical.pdf
protocol="https://"
domain="www.phidgets.com/"
directory="productfiles/3320/3320_0/Documentation/"
remote_file="3320_0_Mechanical.pdf"
file="nema-11-double-shaft.pdf"


if [ ! -f "$file" ]; then
  wget "${protocol}${domain}${directory}${remote_file}" \
    --output-document "${file}" \
    || rm -f "${file}"
fi

protocol="https://"
domain="www.phidgets.com/"
directory="productfiles/3531/3531_0/Documentation/"
remote_file="3531_0_Mechanical.pdf"
file="optical-encoder.pdf"

if [ ! -f "$file" ]; then
  wget "${protocol}${domain}${directory}${remote_file}" \
    --output-document "${file}" \
    || rm -rf "${file}"
fi

# This file is part of combo-breaker-build.

# combo-breaker-build is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# combo-breaker-build is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with challenge-bot.  If not, see <http://www.gnu.org/licenses/>.

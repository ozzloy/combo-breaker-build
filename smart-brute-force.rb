#! /usr/bin/env ruby
require 'pp'

$breaker = {spots: 4,
            # disk at index 0 is front most, attached to dial
            disk: [{spot: 0, spin: -1, gap: 1, nub: 1},
                   {spot: 0, spin: 1, gap: 1, nub: 1},
                   {spot: 0, spin: -1, gap: 1, nub: 1}]}

#---------------------------------------------------
# next_combo({spots: 4, disk: [{spot: 1, spin: -1, gap: 1, nub: 1}]})
# =>
# {spots: 4, disk: [{spot: 0, spin: -1, gap: 1, nub: 1}]}
#---------------------------------------------------
# next_combo({spots: 4, disk: [{spot: 0, spin: -1, gap: 1, nub: 1}]})
# =>
# {spots: 4, disk: [{spot: 3, spin: -1, gap: 1, nub: 1}]}
#---------------------------------------------------
# next_combo({spots: 4, disk: [{spot: 3, spin: -1, gap: 2, nub: 1}]})
# =>
# {spots: 4, disk: [{spot: 1, spin: -1, gap: 1, nub: 1}]}

def next_combo(breaker)
  
end

PP.pp $breaker
proximal = next_combo $breaker
PP.pp proximal
